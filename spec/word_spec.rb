require 'spec_helper'

describe Word do
  let(:term) {'computer'}
  let(:definition) {'the future ruler of us all'}
  let(:new_word) {Word.new(:term => term, :definition => definition)}
  let(:create_word) {Word.create(:term => term, :definition => definition)}
  let(:create_word_stub) {stub_request(:post, "http://localhost:3000/words").
    with(:body => "{\"word\":{\"term\":\"computer\",\"definition\":\"the future ruler of us all\"}}", :headers => {'Content-Type'=>'application/json'}).
    to_return(:body => {'word' => {'definition'=>'the future ruler of us all', 'id'=>6, 'term'=>'computer'}}.to_json)}
  
  let(:delete_request_stub) {stub_request(:delete, "http://localhost:3000/words").
    with(:body => "{\"word\":{\"term\":\"computer\"}}", :headers => {'Content-Type'=>'application/json'})}

  let(:update_request_stub) {stub_request(:put, "http://localhost:3000/words").
    with(:body => "{\"word\":{\"term\":\"computer\",\"definition\":\"the future ruler of us all\"}}", :headers => {'Content-Type'=>'application/json'}).
      to_return(:body => {'word' => {'definition'=>'the future ruler of us all', 'id'=>6, 'term'=>'computer'}}.to_json)}

  context '#initialize' do 
    it 'initializes with a hash containing a word and definition' do 
      new_word.should be_an_instance_of Word
    end
  end

  context 'attr_readers' do
    context '#term' do
      it 'returns the term' do
        new_word.term.should eq term
      end
    end
    context '#definition' do
      it 'returns the definition' do
        new_word.definition.should eq definition
      end
    end
  end

  context '.create' do 
    it 'POSTs the word to the server' do
      stub = create_word_stub
      Word.create(:term => term, :definition => definition)
      stub.should have_been_requested
    end

    it 'returns a parsed response' do 
      sub = create_word_stub
      create_word.should eq new_word
    end
  end

  context '.all' do 
    let(:get_all_response_body) {[{"word"=>{"created_at"=>"2013-03-25T22:52:02Z", "definition"=>"the future ruler of us all", "id"=>20, "term"=>"computer", "updated_at"=>"2013-03-25T22:52:02Z"}}]}
    
    let(:get_all_request_stub) {stub_request(:get, "http://localhost:3000/words").
      to_return(:body => get_all_response_body.to_json)}

    it 'GETs all the words' do
      stub = get_all_request_stub
      Word.all
      stub.should have_been_requested
    end

    it 'returns an array of all the words' do
      stub = get_all_request_stub
      Word.all.first.should be_an_instance_of Word
    end
  end

  context '.delete' do 
    it 'DELETEs the word from the server' do
      stub = delete_request_stub
      Word.delete(:term => term)
      stub.should have_been_requested
    end
  end

  context '.update' do 
    it 'UPDATEs the word' do 
      stub = update_request_stub
      Word.update(:term => term, :definition => definition)
      stub.should have_been_requested
    end
  end

  context '.view' do
    let(:get_word) {{"word"=>{"created_at"=>"2013-03-25T22:52:02Z", "definition"=>"the future ruler of us all", "id"=>20, "term"=>"computer", "updated_at"=>"2013-03-25T22:52:02Z"}}}
    let(:get_word_stub) {stub_request(:get, "http://localhost:3000/words/20").
      to_return(:body => get_word.to_json)}

    it 'GETSs the word' do
      stub = get_word_stub
      Word.view(20)
      stub.should have_been_requested
    end

    it 'returns and instance of word' do 
      stub = get_word_stub
      word = Word.view(20)
      word.should eq Word.new(:term => 'computer', :definition => 'the future ruler of us all')
    end
  end
end