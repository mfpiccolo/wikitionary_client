require './ui_helper'

def welcome
  puts "Welcome to the Github command-line client."
  menu
end

def menu
  choice = nil
  until choice == 'e'
    puts "What would you like to do?"
    puts "Press 'l' to list your words, 'c' to create a new word, 'v' to view a word, 'u' to update a word, or 'd' to delete a word."
    puts "Press 'e' to exit."

    case choice = gets.chomp
    when 'l'
      list
    when 'c'
      create
    when 'v'
      puts 'Enter in the word you would like to view:'
      word = gets.chomp
      view(word)
    when 'u'
      puts 'Enter in the word you would like to update:'
      word = gets.chomp
      update(word)
    when 'd'
      puts 'Enter in the word you would like to delete:'
      word = gets.chomp
      delete(word)
    when 'e'
      exit
    else
      invalid
    end
  end
end

def create
  puts 'Enter in a word:'
  word = gets.chomp
  puts 'Enter in a definition:'
  definition = gets.chomp

  Word.create(:term => word, :definition => definition)
end

def list
  Word.all.each {|word| puts "ID: #{word.id}\n Term: #{word.term}\nDefinition: #{word.definition}\n\n"}
end

def view(id)
  word = Word.view(id)
  puts "\nTerm: #{word.term}\nDefinition: #{word.definition}\n\n"
end

def update(word)
  Word.update(word)
end

def delete(word)
  Word.delete(word)
end

welcome