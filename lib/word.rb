class Word
  attr_reader :term, :definition, :id

  def initialize(options)
    @term = options[:term]
    @definition = options[:definition]
    @id = options[:id]
  end

  def self.create(options)
    post_response = Faraday.post do |request|
      request.url "http://localhost:3000/words"
      request.headers['Content-Type'] = "application/json"
      request.body = {:word => options}.to_json
    end
    body = JSON.parse(post_response.body, :symbolize_names => true)
    Word.new(body[:word])
  end

  def self.all
    get_response = Faraday.get do |request|
      request.url "http://localhost:3000/words"
    end
    body = JSON.parse(get_response.body, :symbolize_names => true)
    body.map {|word_hash| Word.new(word_hash[:word])}
  end

  def self.delete(options)
    delete_response = Faraday.delete do |request|
      request.url "http://localhost:3000/words"
      request.headers['Content-Type'] = "application/json"
      request.body = {:word => options}.to_json
    end
    # JSON.parse(delete_response.body)
  end

  def self.update(options)
    put_response = Faraday.put do |request|
      request.url "http://localhost:3000/words"
      request.headers['Content-Type'] = "application/json"
      request.body = {:word => options}.to_json
    end
    body = JSON.parse(put_response.body, :symbolize_names => true)
    Word.new(body[:word])
  end

  def self.view(id)
    get_response = Faraday.get do |request|
      request.url "http://localhost:3000/words/#{id}"
      request.headers['Content-Type'] = "application/json"
    end
    body = JSON.parse(get_response.body, :symbolize_names => true)
    Word.new(body[:word])
  end

  def ==(other)
    if other.class != self.class
      false
    else
      self.term == other.term && self.definition == other.definition
    end
  end

end